variable "kubernetes" {
    type = map
    sensitive   = true
}

variable "annotations" {
    type = map
}

variable "gitlab_project_id" {
  type = string
}

variable "gitlab_username" {
  type = string
}

variable "gitlab_password" {
  type = string
  sensitive   = true
}

variable "agent_name" {
  type = string
  default = "my-kubernetes-agent"
}

variable "token_name" {
  type    = string
  default = "kas-token"
}

variable "token_description" {
  type    = string
  default = "Token for KAS Agent Authentication"
}

variable "gitlab_graphql_api_url" {
  type    = string
  default = "https://gitlab.com/api/graphql"
}

variable "agent_namespace" {
  type    = string
  default = "gitlab-agent"
}

variable "agent_version" {
  type = string
  default  = "stable"
}
