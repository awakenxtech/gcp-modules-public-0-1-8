resource helm_release wp {

  count = terraform.workspace != "default" ? 1 : 0

  name       = "wp"
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "wordpress"

  cleanup_on_fail = true
  force_update    = true
  recreate_pods   = false
  reset_values    = true

  set {
    name  = "service.type"
    value = "ClusterIP"
  }

}
